package newpackage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import jxl.JXLException;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MyClass {

	public static void main(String args[])
			throws IOException, JXLException, BiffException, FileNotFoundException, InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/home/ubuntu/chromedriver");
		WebDriver driver = new ChromeDriver();
		Sheet s;

		FileInputStream fi = new FileInputStream("/home/ubuntu/Cred.xls");
		Workbook w = Workbook.getWorkbook(fi);
		s = w.getSheet(0);
		int z = s.getRows();
		System.out.println("no of rows:" + z);
		driver.get("http://demo.cognito.gr/public/");

		driver.manage().window().maximize();
		// Entering password protected
		driver.findElement(By.name("site-password-protected")).sendKeys("password1");
		WebElement element = driver.findElement(By.name("site-password-protected"));
		element.submit();

		for (int row = 1; row <= 3; row++) {
			

			// Thumbnail click on home page
			driver.findElement(By.xpath("//*[@id=\"header-img\"]")).click();
			// User login button click
			driver.findElement(By.xpath(
					"/html/body/header/div/div/div/div/div[2]/nav/div/ul/li[5]/div/ul/li/div/div[1]/div[2]/div/div[1]/a"))
					.click();

			Thread.sleep(5000);
			
			driver.findElement(By.id("email")).clear();
			driver.findElement(By.id("password")).clear();
			
			
			String username = s.getCell(0, row).getContents();
			System.out.println("Username " + username);
			driver.findElement(By.id("email")).sendKeys(username);
			
			String password = s.getCell(1, row).getContents();
			System.out.println("Password " + password);
			driver.findElement(By.id("password")).sendKeys(password);
			
			Thread.sleep(1000);

			// Login button click
			driver.findElement(By.xpath("//*[@id=\"UserLoginForm\"]/div[3]/button")).click();
			Thread.sleep(5000);

			// Boolean password_limit_error = false;
			// password_limit_error =
			// driver.findElement(By.className("swal2-confirm")).isDisplayed();

			boolean isDisplayed = false;
			try {
				driver.findElement(By.className("swal2-buttonswrapper"));
				if (driver.findElement(By.className("swal2-buttonswrapper")).isDisplayed()) {
					
					driver.findElement(By.xpath("/html/body/div[21]/div/div[10]/button[1]")).click();
					Thread.sleep(2000);
					driver.findElement(By.className("close")).click();
					Thread.sleep(3000);
					System.out.println("Login Fail !Error Exists");
					isDisplayed = true;
				}

				else {
					System.out.println("Login Pass");
				}
			} catch (NoSuchElementException e) {
				System.out.println("Login Successful !!!");
			}

			if (!isDisplayed) {
				try {
					driver.findElement(By.xpath("//*[@id=\"header-img\"]"));
					driver.findElement(By.xpath("//*[@id=\"header-img\"]")).click();

					driver.findElement(
							By.xpath("//*[@id=\"navbarNavDropdown\"]/ul/li[5]/div/ul/li/div/div[1]/div[3]/div/div/a"))
							.click();

				} catch (NoSuchElementException e) {
					System.out.println("No Header Image, as I dont know what to do with that");
				}
			}
		}

		System.out.println("Above credentials are working!!!");
		driver.close();
	}

}
