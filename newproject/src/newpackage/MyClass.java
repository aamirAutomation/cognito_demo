package newpackage;



import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;



public class MyClass {

	public static void main(String[] args) throws IOException, InterruptedException {

		//checkLogin();
		postNewAd();

	}

	private static void checkLogin() {
		// declaration and instantiation of objects/variables
		// System.setProperty("webdriver.firefox.marionette","C:\\geckodriver.exe");
		// WebDriver driver = new FirefoxDriver();
		// comment the above 2 lines and uncomment below 2 lines to use Chrome
		System.setProperty("webdriver.chrome.driver", "/home/ubuntu/chromedriver");
		WebDriver driver = new ChromeDriver();

		//String baseUrl = "http://demo.cognito.gr/public/";
		String expectedTitle = "Cognito";
		String actualTitle = "";

		//  Wait For Page To Load
		// Put a Implicit wait, this means that any search for elements on the page
		//could take the time the implicit wait is set for before throwing exception 
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Navigate to URL
		driver.get("http://demo.cognito.gr/public/");
		// Maximize the window.
		driver.manage().window().maximize();


		driver.findElement(By.name("site-password-protected")).sendKeys("password1");
		WebElement element = driver.findElement(By.name("site-password-protected")); 
		element.submit();

		driver.findElement(By.xpath("//*[@id=\"header-img\"]")).click();
		driver.findElement(By.xpath("/html/body/header/div/div/div/div/div[2]/nav/div/ul/li[5]/div/ul/li/div/div[1]/div[2]/div/div[1]/a")).click();

		driver.findElement(By.id("email")).sendKeys("user@mail.com");
		driver.findElement(By.id("password")).sendKeys("123456");
		driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/form/div[3]/button")).click();
		// get the actual value of the title
		actualTitle = driver.getTitle();

		/*
		 * compare the actual title of the page with the expected one and print the
		 * result as "Passed" or "Failed"
		 */

		if (actualTitle.contentEquals(expectedTitle)) {
			System.out.println("Test Passed!");
		} else {
			System.out.println("Test Failed");
		}
		// close Fire fox
		//driver.close();
	}

	private static void postNewAd() throws IOException, InterruptedException {

		System.setProperty("webdriver.chrome.driver", "/home/ubuntu/chromedriver");
		WebDriver driver = new ChromeDriver();


		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Navigate to URL
		driver.get("http://demo.cognito.gr/public/");
		// Maximize the window.
		driver.manage().window().maximize();


		driver.findElement(By.name("site-password-protected")).sendKeys("password1");
		WebElement element = driver.findElement(By.className("form-control")); 
		element.submit();
		
		//Thumbnail Click
		driver.findElement(By.xpath("//*[@id=\"header-img\"]")).click();
		
		//User login button click
		driver.findElement(By.xpath("/html/body/header/div/div/div/div/div[2]/nav/div/ul/li[5]/div/ul/li/div/div[1]/div[2]/div/div[1]/a")).click();

		driver.findElement(By.id("email")).sendKeys("resp@mail.com");
		driver.findElement(By.id("password")).sendKeys("123456");
		
		//Login button click
		driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/form/div[3]/button")).click();
		
		Thread.sleep(5000);
		//Post new ad button click
		driver.findElement(By.xpath("//*[@id=\"navbarNavDropdown\"]/ul/li[4]/div/a")).click();
		
		//Page: Important fields
		Thread.sleep(5000);
		Select seller = new Select(driver.findElement(By.name("sellertype_id")));
		seller.selectByVisibleText("Owner");
		driver.findElement(By.xpath("//*[@id=\"residential-tab\"]/div/div[1]/div[1]/div/a/span")).click();
		driver.findElement(By.name("property_title")).sendKeys("Test property");
		driver.findElement(By.name("property_title_GR")).sendKeys("Test property");
		driver.findElement(By.name("property_size")).sendKeys("15000");
		driver.findElement(By.xpath("//*[@id=\"recidentialImportantForm\"]/div[3]/div/div/div/div/div[4]/div[1]/label[1]")).click();
		driver.findElement(By.name("price")).sendKeys("225000");
		Select propertyFor = new Select(driver.findElement(By.name("transactiontype_id")));
		propertyFor.selectByVisibleText("Roommates");
		Select condition = new Select(driver.findElement(By.name("condition_id")));
		condition.selectByVisibleText("Good Condition");
		Select constructionYear = new Select(driver.findElement(By.name("construction_year")));
		constructionYear.selectByVisibleText("2018");
		driver.findElement(By.xpath("//*[@id=\"recidentialImportantForm\"]/div[1]/div[2]/input")).click();
		
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[13]/div/div[10]/button[1]")).click();

		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"user_profile\"]/ul/li[2]/a")).click();

		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[13]/div/div[10]/button[1]")).click();

		//Page: Location
		driver.findElement(By.name("google-auto-complete")).sendKeys("Odisseos 35, Schinias 190 07, Greece");
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[18]/div[1]")).click();
		Thread.sleep(4000);
		driver.findElement(By.name("submit")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("/html/body/div[13]/div/div[10]/button[1]")).click();

		//Page: Publish
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("#user_profile > ul > li:nth-child(7)")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[13]/div/div[10]/button[1]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"location-faccordian\"]/div[2]/div/a[3]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[13]/div/div[10]/button[1]")).click();
			
	}
}
